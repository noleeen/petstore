package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	"gitlab.com/petstore/internal/model"
	"gitlab.com/petstore/internal/modules/auth/controllerAuth"
	"gitlab.com/petstore/internal/modules/auth/serviceAuth"
	"gitlab.com/petstore/internal/modules/pet/controller"
	"gitlab.com/petstore/pkg/responder"
	"gitlab.com/petstore/static"
	"net/http"
)

func NewApiRouter() http.Handler {
	r := chi.NewRouter()

	//авторизация
	authStorage := &model.StorageAuth{UsersMap: map[string]model.User{}}
	token := jwtauth.New("HS256", []byte("randomPass"), nil)

	authService := serviceAuth.NewAuthService(authStorage, token)
	authController := controllerAuth.NewAuthController(authService)

	//наша бизнес-логика
	respond := responder.NewResponder()
	petController := controller.NewPetController(respond)

	r.Post("/register", authController.Register)
	r.Post("/login", authController.Login)
	r.Route("/pet", func(r chi.Router) {
		r.Use(jwtauth.Verifier(token))
		r.Use(jwtauth.Authenticator)

		r.Get("/{petID}", petController.FindPetById)
		r.Get("/findByStatus", petController.FindPetByStatus)

		r.Post("/", petController.AddNewPet)
		r.Post("/{petID}/uploadImage", petController.PetUploadImage)
		r.Post("/{petID}", petController.UpdateWithForm)

		r.Put("/", petController.UpdatePet)

		r.Delete("/{petID}", petController.DeletePet)

	})

	//swagger
	r.Get("/swagger", static.SwaggerUI)
	r.Get("/static/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})

	return r
}
