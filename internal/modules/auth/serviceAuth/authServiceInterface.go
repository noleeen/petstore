package serviceAuth

import "gitlab.com/petstore/internal/model"

type AuthServicer interface {
	CheckRequestRegisterUser(user model.User) error
	AddUser(user model.User) error
	CheckRequestLoginUser(user model.User) error
	GetToken(user model.User) (string, error)
}
