package controllerAuth

import (
	"encoding/json"
	"fmt"
	"gitlab.com/petstore/internal/model"
	"gitlab.com/petstore/internal/modules/auth/serviceAuth"
	"gitlab.com/petstore/pkg/responder"
	"net/http"
)

type AuthController struct {
	service serviceAuth.AuthServicer
	respond responder.Responder
}

func NewAuthController(service serviceAuth.AuthServicer) *AuthController {
	return &AuthController{
		service: service,
		respond: responder.NewResponder(),
	}
}

func (a *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	var registerRequest model.User

	err := json.NewDecoder(r.Body).Decode(&registerRequest)
	if err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	//проверяем не пустой ли запрос и не существует ли такое имя пользователя уже в базе
	if err = a.service.CheckRequestRegisterUser(registerRequest); err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	//добавляем пользователя
	if err = a.service.AddUser(registerRequest); err != nil {
		a.respond.ErrorInternal(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("user successfully created"))
}

func (a *AuthController) Login(w http.ResponseWriter, r *http.Request) {
	var loginRequest model.User

	err := json.NewDecoder(r.Body).Decode(&loginRequest)
	if err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	//проверяем не пустой ли запрос, ищем пользователя в базе и проверяем подходит ли пароль
	if err = a.service.CheckRequestLoginUser(loginRequest); err != nil {
		a.respond.ErrorBadRequest(w, err)
		return
	}

	token, err := a.service.GetToken(loginRequest)
	if err != nil {
		a.respond.ErrorInternal(w, err)
		return
	}

	a.respond.OutputJSON(w, fmt.Sprintf("Bearer %s", token))
}
