package repository

import (
	"fmt"
	"gitlab.com/petstore/internal/model"
	"sync"
)

type PetStorage struct {
	storage       map[int]*model.Pet
	autoIncrement int
	emptyID       []int
	sync.Mutex
}

func NewPetStorage() PetStorager {
	return &PetStorage{
		storage:       make(map[int]*model.Pet, 10),
		autoIncrement: 0,
		emptyID:       make([]int, 0, 7),
	}
}

func (p *PetStorage) AddPet(pet model.Pet) model.Pet {
	p.Lock()
	defer p.Unlock()
	if len(p.emptyID) == 0 {
		p.autoIncrement++
		pet.ID = p.autoIncrement
	} else {
		pet.ID = p.emptyID[0]
		p.emptyID = append(p.emptyID[1:])
	}

	p.storage[pet.ID] = &pet
	return pet
}

func (p *PetStorage) UpdatePet(pet model.Pet) (model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.storage[pet.ID]; ok {
		p.storage[pet.ID] = &pet
		return pet, nil
	}
	return model.Pet{}, fmt.Errorf("not found pet with id: %d\n", pet.ID)
}

func (p *PetStorage) FindById(id int) (*model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if pet, ok := p.storage[id]; ok {
		pet = p.storage[id]
		return pet, nil
	}
	return nil, fmt.Errorf("not found value with id: %d", id)
}

func (p *PetStorage) FindByStatus(status string) []model.Pet {
	p.Lock()
	defer p.Unlock()
	pets := make([]model.Pet, 0, len(p.storage)/2)
	for _, val := range p.storage {
		if val.Status == status {
			pets = append(pets, *val)
		}
	}

	return pets
}

func (p *PetStorage) Delete(id int) error {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.storage[id]; ok {
		p.emptyID = append(p.emptyID, id)
		delete(p.storage, id)
		return nil
	}
	return fmt.Errorf("not found pet with id: %d\n", id)
}

func (p *PetStorage) UploadImage(id int, url string) (*model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if pet, ok := p.storage[id]; ok {
		pet.PhotoUrls = append(pet.PhotoUrls, url)
		return pet, nil
	}
	return nil, fmt.Errorf("not found pet with id: %d\n", id)
}
