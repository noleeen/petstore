package repository

import "gitlab.com/petstore/internal/model"

type PetStorager interface {
	AddPet(pet model.Pet) model.Pet
	UpdatePet(pet model.Pet) (model.Pet, error)
	FindById(id int) (*model.Pet, error)
	FindByStatus(status string) []model.Pet
	Delete(id int) error
	UploadImage(id int, url string) (*model.Pet, error)
}
