package controller

import "net/http"

type PetControllerer interface {
	PetUploadImage(w http.ResponseWriter, r *http.Request)
	AddNewPet(w http.ResponseWriter, r *http.Request)
	UpdatePet(w http.ResponseWriter, r *http.Request)
	FindPetByStatus(w http.ResponseWriter, r *http.Request)
	FindPetById(w http.ResponseWriter, r *http.Request)
	UpdateWithForm(w http.ResponseWriter, r *http.Request)
	DeletePet(w http.ResponseWriter, r *http.Request)
}
