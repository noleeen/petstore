package controller

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	"gitlab.com/petstore/internal/model"
	"gitlab.com/petstore/internal/modules/pet/service"
	"gitlab.com/petstore/pkg/responder"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

type PetController struct {
	service   service.PetServicer
	responder responder.Responder
}

func NewPetController(responder responder.Responder) PetControllerer {
	return &PetController{
		service:   service.NewPetService(),
		responder: responder,
	}
}

func (p *PetController) PetUploadImage(w http.ResponseWriter, r *http.Request) {
	petID, err := strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}

	_ = r.ParseMultipartForm(32 << 20)
	file, m, err := r.FormFile("file")
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	defer file.Close()

	name := strconv.Itoa(petID) + ".*" + filepath.Ext(m.Filename)
	f, err := os.CreateTemp("img/", name)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	defer f.Close()

	_, _ = io.Copy(f, file)

	_, err = p.service.ServiceUploadImage(petID, "/img/"+filepath.Base(f.Name()))
	if err != nil {
		p.responder.ErrorInternal(w, err)
		return
	}

	p.responder.OutputJSON(w, fmt.Sprintf("image for pet wuth id %d uploaded", petID))
}

func (p *PetController) AddNewPet(w http.ResponseWriter, r *http.Request) {
	var pet model.Pet
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	newPet := p.service.ServiceCreate(pet)
	p.responder.OutputJSON(w, newPet)
	//fmt.Sprintf("pet with id %d added", newPet.ID)
}
func (p *PetController) UpdatePet(w http.ResponseWriter, r *http.Request) {
	var pet model.Pet
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	addPet, err := p.service.ServiceUpdate(pet)
	if err != nil {
		p.responder.OutputJSON(w, err.Error())
		return
	}

	p.responder.OutputJSON(w, addPet)
}

func (p *PetController) FindPetByStatus(w http.ResponseWriter, r *http.Request) {
	status := r.URL.Query().Get("status")

	pets := p.service.ServiceGetByStatus(status)
	p.responder.OutputJSON(w, pets)

}

func (p *PetController) FindPetById(w http.ResponseWriter, r *http.Request) {

	petID, err := strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	pet, err := p.service.ServiceGetById(petID)
	if err != nil {
		p.responder.OutputJSON(w, err.Error())
		return
	}
	p.responder.OutputJSON(w, pet)

}

func (p *PetController) UpdateWithForm(w http.ResponseWriter, r *http.Request) {
	petID, err := strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}

	pet, err := p.service.ServiceGetById(petID)
	if err != nil {
		p.responder.OutputJSON(w, err.Error())
		return
	}

	params := r.URL.Query()
	pet.Name = params["name"][0]
	pet.Status = params["status"][0]

	newPet, _ := p.service.ServiceUpdate(*pet)

	p.responder.OutputJSON(w, newPet)

}

func (p *PetController) DeletePet(w http.ResponseWriter, r *http.Request) {
	petID, err := strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}

	err = p.service.ServiceDelete(petID)
	if err != nil {
		p.responder.OutputJSON(w, err.Error())
		return
	}
	p.responder.OutputJSON(w, fmt.Sprintf("pet with id %d deleted", petID))
}
