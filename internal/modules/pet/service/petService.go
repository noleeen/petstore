package service

import (
	"gitlab.com/petstore/internal/model"
	"gitlab.com/petstore/internal/modules/pet/repository"
)

type PetService struct {
	storage repository.PetStorager
}

func NewPetService() PetServicer {
	return &PetService{storage: repository.NewPetStorage()}
}

func (p *PetService) ServiceCreate(pet model.Pet) model.Pet {
	return p.storage.AddPet(pet)
}

func (p *PetService) ServiceUpdate(pet model.Pet) (model.Pet, error) {
	return p.storage.UpdatePet(pet)
}

func (p *PetService) ServiceDelete(petId int) error {
	return p.storage.Delete(petId)
}

func (p *PetService) ServiceGetById(petId int) (*model.Pet, error) {
	return p.storage.FindById(petId)
}

func (p *PetService) ServiceGetByStatus(status string) []model.Pet {
	return p.storage.FindByStatus(status)
}

func (p *PetService) ServiceUploadImage(petID int, url string) (*model.Pet, error) {
	return p.storage.UploadImage(petID, url)
}
