package service

import "gitlab.com/petstore/internal/model"

type PetServicer interface {
	ServiceCreate(pet model.Pet) model.Pet
	ServiceUpdate(pet model.Pet) (model.Pet, error)
	ServiceDelete(petId int) error
	ServiceGetById(petId int) (*model.Pet, error)
	ServiceGetByStatus(status string) []model.Pet
	ServiceUploadImage(petID int, url string) (*model.Pet, error)
}
