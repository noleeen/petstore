package static

import (
	"bytes"
	"gitlab.com/petstore/internal/model"
)

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// Security:
//  - Bearer: []
// responses:
//     200: petAddResponse
//400: description: Invalid input

// Show added struct.
//
//swagger:response petAddResponse
type petAddResponse2 struct {
	// in:body
	Body model.Pet
}

// swagger:parameters petAddRequest
type petAddRequest2 struct {
	// Example: {"id": 0, "category": {"id": 0,"name": "string"},"name": "doggie", "photoUrls": [ "string" ],"tags": [{"id": 0,"name": "string"}],"status": "available"}
	// in:body
	Body model.Pet
}

// swagger:route GET /pet/{petID} pet petGetByIDRequest
// Получение питомца по ID.
// Security:
//  - Bearer: []
// responses:
//  200: petGetByIDResponse200
//	400: description: Invalid input

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	//
	// required:true
	// In: path
	ID string `json:"petID"`
}

// swagger:response petGetByIDResponse200
type petGetByIDResponse200 struct {
	// in:body
	Body model.Pet
}

// swagger:route GET /pet/findByStatus pet petListByStatusRequest
// Поиск списка питомцев по статусу.
// Security:
//   - Bearer: []
// responses:
//   200: petListByStatusResponse

// swagger:parameters petListByStatusRequest
type petListByStatusRequest struct {
	// in:query
	// required:true
	// type:string
	// enum:available,pending,sold
	// description: Status values that need to be considered for filter
	Status string `json:"status"`
}

// List of pets.
// swagger:response petListByStatusResponse
type petListByStatusResponse struct {
	// in:body
	Body []model.Pet
}

// swagger:route PUT /pet pet petUpdate
// Обновление питомца.
// Security:
//   - Bearer: []
// responses:
// 200: petUpdateResponse200
// 400: description: Invalid input

// swagger:parameters petUpdate
type petUpdateRequest struct {
	//in:body
	// Example: {"id": 0, "category": {"id": 0,"name": "string"},"name": "doggie", "photoUrls": [ "string" ],"tags": [{"id": 0,"name": "string"}],"status": "available"}
	Body model.Pet
}

// Show added struct.
// swagger:response petUpdateResponse200
type petUpdateResponse200 struct {
	// in:body
	Body model.Pet
}

// swagger:route DELETE /pet/{petID} pet petDeleteRequest
// Удаление питомца.
// Security:
//   - Bearer: []
// responses:
// 200: petDeleteRequest
// 400: description: Invalid input

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// ID of pet
	// required:true
	// In: path
	ID string `json:"petID"`
}

// Show deleted id.
// swagger:response petDeleteRespond
type petDeleteRespond struct {
	//in:body
	Body string
}

// swagger:route POST /pet/{petID}/uploadImage pet petUploadImageRequest
// Добавление изображения по ID.
// Security:
//   - Bearer: []
// responses:
//  200: petUploadImageResponse
//	400: description: Invalid input

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct {
	// ID of pet
	// required: true
	// in:path
	ID string `json:"petID"`

	// file to upload
	// in: formData
	// swagger:file
	File *bytes.Buffer `json:"file"`
}

// Show upload pet.
//
//swagger:response petUploadImageResponse
type petUploadImageResponse struct {
	//in:body
	Body model.Pet
}

// swagger:route POST /pet/{petID} pet petUpdateWithFormRequest
// Обновление данных питомца по ID используя форму.
// Security:
//  - Bearer: []
// responses:
//  200: petUpdateWithFormResponse

// swagger:parameters petUpdateWithFormRequest
type petUpdateWithFormRequest struct {
	// ID of pet
	// required: true
	// in:path
	ID string `json:"petID"`

	// new name
	// required: true
	// in: query
	Name string `json:"name"`

	// new status
	// required: true
	// in: query
	Status string `json:"status"`
}

// Show update pet.
//
//swagger:response petUpdateWithFormResponse
type petUpdateWithFormResponse struct {
	// in:body
	Body model.Pet
}
