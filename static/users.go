package static

import "gitlab.com/petstore/internal/model"

//swagger:route POST /register user RegisterRequest
// Регистрация нового пользователя.
// security:
// - basic
// Responses:
// 200: RegisterResponse

//swagger:parameters RegisterRequest
type RegisterRequest struct {
	//R
	//in:body
	//required: true
	//example: {"name":"testUser", "password":"testPass"}
	Body model.User
}

//swagger:response RegisterResponse
type RegisterResponse struct {
	//in:body
	Body string
}

// swagger:route POST /login user LoginRequest
// Аутентификация пользователя.
// security:
// - basic
//Responses:
// 200: LoginResponse

//swagger:parameters LoginRequest
type LoginRequest struct {
	//L
	//in:body
	//required: true
	//example: {"name":"testUser", "password":"testPass"}
	Body model.User
}

//swagger:response LoginResponse
type LoginResponse struct {
	//in:body
	Body string
}
