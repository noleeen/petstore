// Package static PetStore.
//
// This is a sample server Petstore server.
//
//	 Schemes: http, https
//	 BasePath: /
//	 Version: 1.0.0
//
//	 Consumes:
//	 - application/json
//	 - multipart/form-data
//
//	 Produces:
//	 - application/json
//
//	Security:
//	- Bearer
//
//	SecurityDefinitions:
//	  Bearer:
//	    type: apiKey
//	    name: Authorization
//	    in: header
//
// swagger:meta
package static

//go:generate swagger generate spec -o ./swagger.json --scan-models
